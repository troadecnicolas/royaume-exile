import { createApp } from 'vue'
import { devtools } from 'vue'
import App from './App.vue'
import store from './store'

let app = createApp(App);

app.use(devtools);

app.use(store);

app.mount('#app');


