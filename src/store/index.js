import { createStore } from 'vuex'

export default createStore({
  state: {
    ressources: {
      bois: {
        quantity: 0,
        asked: 0,
        auto: false,
      },
      pierre: {
        quantity: 0,
        asked: 0,
        auto: false,
      },
      or: {
        quantity: 0,
        asked: 0,
        auto: false,
      },
      fer: {
        quantity: 0,
        asked: 0,
        auto: false,
      },
      tissus: {
        quantity: 0,
        asked: 0,
        auto: false,
      },
      nourriture: {
        quantity: 100,
        asked: 0,
        auto: false,
      },
      acier: {
        quantity: 0,
        asked: 0,
        auto: false,
      },
    },
    workers: [
      {
        name: "Tatiana",
        status: "idle",
        age: 32,
        taille: 176,
        sexe: "Femme",
        skills: {
          coupe: 3,
          taille: 7,
          metallurgie: 2,
          couture: 6,
          cuisine: 7,
          minage: 2,
        },
        mealTime: {
          morning: 6,
          evening: 19,
        },
        task: {
          itemName: String,
          quantityToGather: 0,
        }
      },
      {
        name: "Rodolphe",
        status: "idle",
        age: 56,
        taille: 155,
        sexe: "Homme",
        skills: {
          coupe: 10,
          taille: 1,
          metallurgie: 3,
          couture: 12,
          cuisine: 2,
          minage: 6,
        },
        mealTime: {
          morning: 7,
          evening: 20,
        },
        task: {
          itemName: String,
          quantityToGather: 0,
        }
      },
      {
        name: "Ursula",
        status: "idle",
        age: 17,
        taille: 185,
        sexe: "Femme",
        skills: {
          coupe: 6,
          taille: 10,
          metallurgie: 9,
          couture: 1,
          cuisine: 9,
          minage: 11,
        },
        mealTime: {
          morning: 8,
          evening: 20,
        },
        task: {
          itemName: String,
          quantityToGather: 0,
        }
      },
      {
        name: "Leon",
        status: "idle",
        age: 22,
        taille: 165,
        sexe: "Homme",
        skills: {
          coupe: 5,
          taille: 9,
          metallurgie: 5,
          couture: 4,
          cuisine: 11,
          minage: 7,
        },
        mealTime: {
          morning: 7,
          evening: 19,
        },
        task: {
          itemName: String,
          quantityToGather: 0,
        }
      },
    ],
    tickets: {
      /* {
        id: Number,
        quantity: Number,
        itemName: String,
        status: String,
        peon: String,
      } */
    },
    totalTickets: 0,
    city: {
      name: undefined,
      country: undefined,
    },
    days: 0,
    time: {
      minutes: 0,
      hours: 0,
      speed: 1,
    },
    timeZone: false,
    weather: {
      temp: 0,
      humidity: 0,
      windSpeed: 0,
    },
  },
  mutations: {
    askRessource(state, payload) {
      const name = payload.name;
      const quantity = payload.quantity;

      if (state.ressources[name].auto) {
        state.ressources[name].asked = quantity;
        state.ressources[name].auto = false;
      } else {
        state.ressources[name].asked = state.ressources[name].asked + quantity;
      }
    },
    autoRessource(state, itemName) {
      state.ressources[itemName].auto = true;
    },
    busy(state, peonName) {
      const worker = state.workers.find(w => w.name === peonName);
      worker.status = 'busy';
    },
    changeWeather(state) {
      const hours = state.time.hours;
      if ((hours >= 6) && (hours < 18)) {
        state.weather = {
          ...state.weather,
          temp: state.weather.temp + 0.05,
        }
      } else if ((hours >= 18) || (hours < 2)) {
        state.weather = {
          ...state.weather,
          temp: state.weather.temp - 0.05,
        }
      } else if ((hours >= 2) && (hours < 6)) {
        state.weather = {
          ...state.weather,
          temp: state.weather.temp - 0.05,
        }
      }
    },
    changeSpeed(state, speed) {
      state.time = {
        ...state.time,
        speed: speed,
      }
    },
    cityChosen(state, payload) {
      state.city = {
        ...state.city,
        name: payload.name,
        country: payload.country,
      }
    },
    createTicket(state, payload) {
      const ticket = {
        id: Number,
        quantity: Number,
        itemName: String,
        status: String,
        peon: String,
        ticketKey: String,
      }
      ticket.quantity = payload.quantity;
      ticket.itemName = payload.name;
      ticket.status = 'waiting';
      ticket.peon = workerWithHighestSkill(state, payload.name);

      state.totalTickets++;
      ticket.id = state.totalTickets;
      let ticketId = ticket.id;
      let ticketKey = 'ticket' + ticketId.toString();
      ticket.ticketKey = ticketKey;
      state.tickets = {
        ...state.tickets,
        [ticketKey]: ticket,
      };
      function workerWithHighestSkill(state, itemName) {
        let skillRequired;
        switch (itemName) {
          case 'bois':
            skillRequired = 'coupe';
            break;
          case 'pierre':
            skillRequired = 'taille';
            break;
          case 'or':
            skillRequired = 'minage'
            break;
          case 'fer':
            skillRequired = 'minage'
            break;
          case 'tissus':
            skillRequired = 'couture'
            break;
          case 'nourriture':
            skillRequired = 'cuisine'
            break;
          case 'acier':
            skillRequired = 'metallurgie'
            break;
          default:
            break;
        }

        const workersSkills = state.workers.map((w) => w.skills[skillRequired]);

        let highestSkill = Math.max(...workersSkills);

        let workerHighestSkill = state.workers.find((w) => w.skills[skillRequired] === highestSkill);

        if (workerHighestSkill.status === 'busy') {
          let isAWorkerFree = state.workers.find(w => w.status === 'idle');
          if (isAWorkerFree) {
            let workerNotFound = true;
            while (workerNotFound) {
              let indexOfHigestSkill = workersSkills.indexOf(highestSkill);
              workersSkills.splice(indexOfHigestSkill, 1),
                highestSkill = Math.max(...workersSkills);
              workerHighestSkill = state.workers.find((w) => w.skills[skillRequired] === highestSkill);
              if (workerHighestSkill.status === 'busy') {
                workerNotFound = true;
              } else
                workerNotFound = false;
            }
          }

        }
        return workerHighestSkill.name;
      }
    },
    deleteTicket(state, ticketToDelete) {
      delete state.tickets[ticketToDelete.ticketKey];
    },
    incrementDay(state) {
      state.days++;
    },
    idle(state, peonName) {
      const worker = state.workers.find(w => w.name === peonName);
      worker.status = 'idle';
    },
    initWeather(state, weather) {
      state.weather = {
        ...state.weather,
        temp: weather.temp,
        humidity: weather.humidity,
        windSpeed: weather.windSpeed,
      }
    },
    mealtime(state) {
      state.ressources.nourriture = {
        ...state.ressources.nourriture,
        quantity: state.ressources.nourriture.quantity - 1,
      }
    },
    reattribute(state, payload) {
      console.log(payload)
      state.tickets[payload.ticket.ticketKey] = {
        ...state.tickets[payload.ticket.ticketKey],
        peon: payload.peon,
      }
    },
    ressourcesInit(state, newRessources) {
      Object.entries(state.ressources).map(oldRessource => {
        newRessources.filter(
          (newRessource) => {
            if (newRessource.name === oldRessource[0]) {
              oldRessource[1].quantity = newRessource.quantity;
            }
          }
        );
      })
    },
    setTimeZone(state, timeZone) {
      state.timeZone = timeZone;
    },
    ticketInProgress(state, ticket) {
      state.tickets[ticket.ticketKey] = {
        ...state.tickets[ticket.ticketKey],
        status: 'inProgress',
      }
    },
    time(state, newTime) {
      state.time.minutes = newTime.minutes;
      state.time.hours = newTime.hours;
    },
    work(state, payload) {
      state.tickets[payload.ticket.ticketKey] = {
        ...state.tickets[payload.ticket.ticketKey],
        quantity: state.tickets[payload.ticket.ticketKey].quantity - 1,
      }
      state.ressources[payload.ticket.itemName] = {
        ...state.ressources[payload.ticket.itemName],
        asked: state.ressources[payload.ticket.itemName].asked - 1,
        quantity: state.ressources[payload.ticket.itemName].quantity + 1,
      }
    },
  },


})


